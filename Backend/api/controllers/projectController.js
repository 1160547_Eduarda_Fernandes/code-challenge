'use strict';


var mongoose = require('mongoose'),
    Project = mongoose.model('Projects');

exports.list_all_projects = function (req, res) {
    Project.find({}, function (err, proj) {
        if (err)
            res.send(err);
        res.json(proj);
    });
};

exports.create_a_project = function (req, res) {
    var new_project = new Project(req.body);
    new_project.save(function (err, proj) {
        if (err)
            res.send(err);
        res.json(proj);
    });
};

exports.find_a_project = function(req, res) {
    Project.findById(req.params.projectId, function(err, proj) {
      if (err)
        res.send(err);
      res.json(proj);
    });
  };

exports.update_a_project = function (req, res) {
    Project.findOneAndUpdate({ _id: req.params.projectId }, req.body, { new: true }, function (err, proj) {
        if (err)
            res.send(err);
        res.json(proj);
    });
};


exports.delete_a_project = function (req, res) {
    Project.remove({
        _id: req.params.projectId
    }, function (err, proj) {
        if (err)
            res.send(err);
        res.json({ message: 'Project successfully deleted' });
    });
};