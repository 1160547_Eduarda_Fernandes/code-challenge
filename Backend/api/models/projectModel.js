'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Task = mongoose.model('Tasks');


var ProjectSchema = new Schema({
  description: {
    type: String,
    required: 'description of the Project'
  },
  tasks: {
    type: Array,
    items: {
      type: Task.description
    },
    required: 'tasks of the project'
  }
});

module.exports = mongoose.model('Projects', ProjectSchema);