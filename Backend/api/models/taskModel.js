'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var TaskSchema = new Schema({
  description: {
    type: String,
    required: 'description of the Task',
    unique: true
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  finished_date: {
    type: Date,
    default: new Date(new Date().getTime()+(10*24*60*60*1000))
  },
  status: {
    type: [{
      type: String,
      enum: [true, false]
    }],
    default: [false]
  }
});

module.exports = mongoose.model('Tasks', TaskSchema);