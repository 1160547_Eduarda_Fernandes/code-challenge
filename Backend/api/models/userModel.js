'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var UserSchema = new Schema({
  id: {
    type: String,
    required: 'id of the user'
  },
  username: {
    type: String,
    required: 'username of the user'
  },
  passowrd: {
    type: String,
    required: 'password of the user'
  }
});

module.exports = mongoose.model('Users', UserSchema);