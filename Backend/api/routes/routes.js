'use strict';
module.exports = function (app) {
    var usersList = require('../controllers/userController');
    var projectList = require('../controllers/projectController');
    var taskList = require('../controllers/taskController');

    // usersList Routes
    app.route('/users')
        .get(usersList.list_all_users)
        .post(usersList.create_a_user);


    app.route('/users/:userId')
        .get(usersList.find_a_user)



    // projectList Routes
    app.route('/projects')
        .get(projectList.list_all_projects)
        .post(projectList.create_a_project);


    app.route('/projects/:projectId')
        .get(projectList.find_a_project)
        .put(projectList.update_a_project)
        .delete(projectList.delete_a_project)



    // taskList Routes
    app.route('/tasks')
        .get(taskList.list_all_tasks)
        .post(taskList.create_a_task);


    app.route('/tasks/:taskId')
        .put(taskList.update_a_task)
        .delete(taskList.delete_a_task);
};