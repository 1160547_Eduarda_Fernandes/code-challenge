var express = require('express'),
  cors = require('cors'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  Task = require('./api/models/taskModel'), 
  Project = require('./api/models/projectModel'),
  User = require('./api/models/userModel'), 
  bodyParser = require('body-parser');

  app.use(cors())

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/Bolttechdb');


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routes = require('./api/routes/routes');
routes(app);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);