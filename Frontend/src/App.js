import './App.css';
import Routes from './Routes';

export const BASE_URL = 'http://localhost:3000';

function App() {
  return (
    <Routes />
  );
}

export default App;
