import React, { Component } from 'react';
import { BASE_URL } from '../../App';
import axios from 'axios';
import '../../styles/general.css';
import '../../styles/table.css';
import '../../styles/modal.css';
import edit from '../../img/edit.svg';


class addTask extends Component {

    constructor(props) {
        super(props);

        this.state = {
            projects: [],
            openModal: false,
            description: '',
        };
        this.handleOpen = this.handleOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    componentDidMount() {
        if (this.props.setTask) {
            let target = this.props.targetTask;

            this.setState({
                description: target.description
            })
        }
    }

    changeHandler = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    handleOpen() {
        this.setState({
            openModal: true
        });
    }

    handleClose() {
        this.setState({
            openModal: false,
        });
    }

    handleAddNewTask = (e) => {

        let updateProjectTasks = this.props.projectTasks;

        e.preventDefault()
        axios
            .post(`${BASE_URL}/tasks`, {
                description: this.state.description,
                // tasks: this.state.projectTasks
            }).then(res => {

                updateProjectTasks.push(this.state.description);

                axios.put(`${BASE_URL}/projects/${this.props.projectId}`, {
                    tasks: updateProjectTasks
                }).then(res => {
                    window.location = `/my-project/${this.props.projectId}`;
                    console.log(res);
                })
            }).catch(error => {
                console.log(error);
            });
    }

    handleSetTask = (e) => {

        let updateProjectTasks = this.props.projectTasks;

        e.preventDefault()

        axios
            .put(`${BASE_URL}/tasks/${this.props.targetTask._id}`, {
                description: this.state.description
                // tasks: this.state.projectTasks
            }).then(res => {

                updateProjectTasks.push(this.state.description);

                updateProjectTasks.filter(t => t.description !== this.props.targetTask.description)

                axios.put(`${BASE_URL}/projects/${this.props.projectId}`, {
                    tasks: updateProjectTasks
                }).then(res => {
                    window.location = `/my-project/${this.props.projectId}`;
                    console.log(res);
                })
            }).catch(error => {
                console.log(error);
            });
    }

    render() {

        const { description } = this.state;

        return (
            <div >
                <div onClick={this.handleOpen}>
                    <div className={this.props.setTask ? "close" : "open"}>
                        <button className="openModal">Add Task</button>
                    </div>
                    <div className={this.props.setTask ? "open" : "close"}>
                        <img src={edit} alt="pencil" style={{ height: "17px", cursor: "pointer" }} />
                    </div>
                </div>
                <div className={(this.state.openModal ? 'open' : 'close')}>
                    <div className="backgoundsWrapper">
                        <div className="windowBackground">
                        </div>
                        <div className="modalBackground">
                            <div className="modalNameWrapper">
                                <div className="alignModalWrapper">
                                    <p className="title">Add New Task</p>
                                </div>
                            </div>
                            <div className="modalAttributesWrapper">
                                <div className="attributesWrapper">
                                    <div>
                                        <p className="modalAttribute">Description</p>
                                        <input
                                            type="text"
                                            className="modalField"
                                            name="description"
                                            required={true}
                                            id="descriptionText" value={description}
                                            onChange={this.changeHandler}
                                            placeholder="  insert the name of the task here...">
                                        </input>
                                    </div>
                                </div>

                                <div className="attributesWrapper">
                                    <div>
                                        <button className="modalCancelBtn" onClick={this.handleClose}>Close</button>
                                    </div>
                                    <div>
                                        <button className="modalAddBtn" onClick={this.props.setTask ? this.handleSetTask : this.handleAddNewTask}>Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

    }
}

export default addTask;