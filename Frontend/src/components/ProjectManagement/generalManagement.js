import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { BASE_URL } from '../../App';
import axios from 'axios';
import '../../styles/general.css';
import '../../styles/table.css';
import signOut from '../../img/signOut.svg';
import edit from '../../img/edit.svg';
import remove from '../../img/remove.svg';
import AddProject from './addProject';

class generalManagement extends Component {

    constructor(props) {
        super(props);

        this.state = {
            projects: [],
        };

        this.removeProject = this.removeProject.bind(this);
    }

    componentDidMount() {
        fetch(`${BASE_URL}/projects/`)
            .then(res => res.json())
            .then(res => {
                this.setState({
                    projects: res,
                });
            }).catch(error => {
                console.log(error);
            });
    }

    removeProject(project) {
        axios
            .delete(`${BASE_URL}/projects/${project._id}`)
            .then(res => {
                console.log(res)
                window.location = '/my-projects';
            }).catch(error => {
                console.log(error);
            })
    }


    render() {
        return (
            <div className='App'>
                <div className="topWrapper">
                    <p className="title">My Projects</p>
                    <div className='loginPageWrapper'>
                        <img src={signOut} alt="signOut" height="16px"></img>
                        <p className="signOut">Log Out</p>
                    </div>
                </div>
                <div className="division"></div>
                <div className='btnWrapper'>
                    <AddProject />
                </div>
                <div className="tableWrapper">
                    <div className="tableHeadersWrapper">
                        <div className="headers">
                            <p className='headerAttribute'>Name</p>
                            <p className='headerAttribute'>Options</p>
                        </div>
                    </div>
                </div>
                <div className='resultsWrapper'>
                    {this.state.projects.map((prj) =>
                        <div className="tableAttributesWrapper" key={prj.id}>
                            <p className="attribute">{prj.description}</p>
                            <div className='optionsWrapper'>
                                <NavLink to={`/my-project/${prj._id}`} > <img src={edit} alt="pencil" style={{ height: "19px", cursor: "pointer" }} /></NavLink>
                                <img src={remove} alt="trash" style={{ height: "19px", cursor: "pointer" }} onClick={this.removeProject.bind(this, prj)} />
                            </div>
                        </div>
                    )}
                </div>
            </div>
        );

    }
}

export default generalManagement;