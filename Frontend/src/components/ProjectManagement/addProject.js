import React, { Component } from 'react';
import { BASE_URL } from '../../App';
import axios from 'axios';
import '../../styles/general.css';
import '../../styles/table.css';
import '../../styles/modal.css';

class addProject extends Component {

    constructor(props) {
        super(props);

        this.state = {
            projects: [],
            openModal: false,
            description: '',
        };
        this.handleOpen = this.handleOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    changeHandler = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    handleOpen() {
        this.setState({
            openModal: true
        });
    }

    handleClose() {
        this.setState({
            openModal: false,
        });
    }

    handleAddNewProject = (e) => {
        e.preventDefault()
        axios
            .post(`${BASE_URL}/projects`, {
                description: this.state.description,
                // tasks: this.state.projectTasks
            }).then(res => {
                window.location = '/my-projects';
                console.log(res);
            }).catch(error => {
                console.log(error);
            });
    }

    render() {

        const { description } = this.state;

        return (
            <div >
                <div onClick={this.handleOpen}>
                    <button className="openModal">Add Project</button>
                </div>
                <div className={(this.state.openModal ? 'open' : 'close')}>
                    <div className="backgoundsWrapper">
                        <div className="windowBackground"></div>
                        <div className="modalBackground">
                            <div className="modalNameWrapper">
                                <div className="alignModalWrapper">
                                    <p className="title">Add New Project</p>
                                </div>
                            </div>
                            <div className="modalAttributesWrapper">
                                <div className="attributesWrapper">
                                    <div>
                                        <p className="modalAttribute">Description</p>
                                        <input
                                            type="text"
                                            className="modalField"
                                            name="description"
                                            required={true}
                                            id="descriptionText" value={description}
                                            onChange={this.changeHandler}
                                            placeholder="  insert the name of the project here...">
                                        </input>
                                    </div>
                                </div>
                                <div className="attributesWrapper">
                                    <div>
                                        <button className="modalCancelBtn" onClick={this.handleClose}>Close</button>
                                    </div>
                                    <div>
                                        <button className="modalAddBtn" onClick={this.handleAddNewProject}>Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default addProject;