import React, { Component } from 'react';
import { BASE_URL } from '../../App';
import axios from 'axios';
import '../../styles/general.css';
import '../../styles/table.css';
import '../../styles/modal.css';
import AddTask from '../TaskManagement/addEditTask';
import remove from '../../img/remove.svg';
import Tooltip from '@material-ui/core/Tooltip';
import { NavLink } from 'react-router-dom';

class editProject extends Component {

    constructor(props) {
        super(props);

        this.state = {
            project: [],
            description: '',
            projectTasks: [],
            allTasks: [],
            originalProjectTasks: []
        };
        this.removeTask = this.removeTask.bind(this);
        this.changeHandlerCheckbox = this.changeHandlerCheckbox.bind(this);
    }

    componentDidMount() {
        const projectId = this.props.match.params.id;

        fetch(`${BASE_URL}/projects/${projectId}`)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    description: json.description,
                    projectTasks: json.tasks
                })
            }).catch(error => {
                console.log(error);
            });

        fetch(`${BASE_URL}/tasks/`)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    allTasks: json,
                }, () => {
                    let getTasks = this.state.allTasks.filter(x => this.state.projectTasks.includes(x.description))
                    this.setState({
                        originalProjectTasks: getTasks
                    })
                })
            }).catch(error => {
                console.log(error);
            });
    }

    changeHandler = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    handleSaveProject = (e) => {
        e.preventDefault()
        axios
            .put(`${BASE_URL}/projects/${this.props.match.params.id}`, {
                description: this.state.description,
                tasks: this.state.projectTasks
            }).then(res => {
                window.location = '/my-projects';
                console.log(res);
            }).catch(error => {
                console.log(error);
            });
    }
    removeTask(task) {
        axios
            .delete(`${BASE_URL}/tasks/${task._id}`)
            .then(res => {
                console.log(res)
                window.location = `/my-project/${this.props.match.params.id}`;
            }).catch(error => {
                console.log(error);
            })
    }

    changeHandlerCheckbox(task) {
        let updateProjectTasks = this.state.projectTasks;
        axios
            .put(`${BASE_URL}/tasks/${task._id}`, {
                status: "true"
            }).then(res => {
                updateProjectTasks.push(this.state.description);
                updateProjectTasks.filter(t => t.description !== task.description)
                axios.put(`${BASE_URL}/projects/${this.props.match.params.id}`, {
                    tasks: updateProjectTasks
                }).then(res => {
                    window.location = `/my-project/${this.props.match.params.id}`;
                    console.log(res);
                })
            }).catch(error => {
                console.log(error);
            });
    }

    render() {

        return (
            <div className='App'>
                <div className="topWrapper">
                    <p className="title">{this.state.description}</p>
                </div>
                <div className="division"></div>
                <div className='btnWrapper'>
                    <AddTask projectId={this.props.match.params.id} projectTasks={this.state.projectTasks} setTask={false} />
                </div>
                <div className="tableWrapper">
                    <div className="tableHeadersWrapper">
                        <div className="headers">
                            <p className='headerAttribute'>To Do</p>
                            <p className='headerAttribute'>Done</p>
                        </div>
                    </div>
                </div>
                <div className='resultsWrapper'>
                    {this.state.originalProjectTasks.map((tk) =>
                        <div className="tableAttributesWrapperE" key={tk.id}>
                            <div className={tk.status[0] === "false" ? "open" : "close"}>
                                <div className='tasksTableWrapper'>
                                    <input type="checkbox" id={tk.id} name="task" value={tk.description} onChange={this.changeHandlerCheckbox.bind(this, tk)} style={{ marginTop: "6%" }} />
                                    <Tooltip title={<h3 style={{ maxWidth: "250px" }}>Deadline: {tk.finished_date.substring(0, 10)}</h3>} arrow placement="top">
                                        <label htmlFor="ativo" className="attribute">{tk.description}</label>
                                    </Tooltip>
                                    <div className='editOptionsWrapper'>
                                        <AddTask projectId={this.props.match.params.id} projectTasks={this.state.projectTasks} setTask={true} targetTask={tk} />
                                        <img src={remove} alt="trash" style={{ height: "17px", cursor: "pointer" }} onClick={this.removeTask.bind(this, tk)} />
                                    </div>
                                </div>
                            </div>
                            <div className={tk.status[0] === "true" ? "open" : "close"}>
                                <div className='tasksTableWrapperD'>
                                    <input type="checkbox" id={tk.id} name="task" value={tk.description} checked={true} disabled={true} style={{ marginTop: "10%" }} />
                                    <label htmlFor="ativo" className="attribute">{tk.description}</label>
                                </div>
                            </div>
                        </div>
                    )}
                </div>
                <div className='backButton'>
                    <NavLink to={`/my-projects/`}><button className="openModal" >Back</button></NavLink>
                </div>
            </div>
        );

    }
}

export default editProject;