import React, { Component } from 'react';
import background from '../../img/login.png';

import '../../styles/general.css';

class Login extends Component {

    componentDidMount(){
        this.goMyProjects = this.goMyProjects.bind(this);
    }

    goMyProjects(){
        window.location = '/my-projects';
    }

    render() {

        return (
            <div className='App'>
                <div className='loginPageWrapper'>
                    <img src={background} alt="background" className='backgroundImage'></img>
                    <div className='loginFormWrapper'>
                        <p className='title'>Bolttech Code Challenge</p>
                        <button className="openModal" onClick={this.goMyProjects}>Enter</button>
                    </div>
                </div>


            </div>
        );

    }
}

export default Login;