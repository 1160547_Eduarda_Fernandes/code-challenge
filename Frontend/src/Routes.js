import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Login from './components/Login/Login';
import ProjectManagement from './components/ProjectManagement/generalManagement';
import editProject from "./components/ProjectManagement/editProject";

export default class Routes extends Component {

    render() {
        return (
            <Router >
                <Route path="/" />
                <Switch>
                    <Route exact path="/"><Login /></Route>
                    <Route path="/my-projects"> <ProjectManagement /></Route>
                    <Route path="/my-project/:id" component={editProject}></Route>
                </Switch>
            </Router>
        )
    }
}

//<Route path="/my-projects"> <ProjectManagement /></Route>